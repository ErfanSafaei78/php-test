<?php
    function myF ($max) {
        for ($i = 1; $i < $max; $i++) {
            $stop = yield $i;
            if ($stop === 'stop'){
                return;
            }
        }
    }
    
    $myG = myF(PHP_INT_MAX);
    foreach ($myG as $value) {
        if ($value === 15000){
            $myG -> send('stop');
        }
        echo "value is {$value} <br>";
    }
?>
